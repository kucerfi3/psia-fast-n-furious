import socket
import time
import threading
import concurrent.futures
import random
from utils import *

WINDOW_SIZE = 64
METAPACKET_TYPE = 0
DATAPACKET_TYPE = 1
STOPPACKET_TYPE = 255

TIMEOUT_ACK = 0.1

# TARGET_IP = "185.63.96.172"
TARGET_IP = "127.0.0.1"
TARGET_PORT = 49853
#TARGET_PORT = 49854
SOURCE_PORT = 61234
CHUNK_LEN = 1015
FILENAME = "Receiver.py"

packet_succ_lock = threading.Lock()
file_lock = threading.Lock()
socket_lock = threading.Lock()
packets_sent_successfully = set()

'''''''''
def await_ack(main_socket, expected_type, expected_number):
    try:
        data, server = main_socket.recvfrom(CHUNK_LEN)
        # print(f'Received {data}')
        expected_num_big_end = expected_number.to_bytes(4, byteorder="big")
        if crc32_checker(data) and data[4] == expected_type and data[5:9] == expected_num_big_end:
            return 0
        else:
            return -2
    except socket.timeout:
        return -1
'''''''''''


def await_ack(main_socket, expected_type, expected_number):
    expected_num_big_end = expected_number.to_bytes(4, byteorder="big")

    if expected_num_big_end in packets_sent_successfully:
        return 0

    try:
        with socket_lock:
            data, server = main_socket.recvfrom(CHUNK_LEN)
        #print(f"Expected : {expected_number}\tGot: {data}")
        if crc32_checker(data) and data[4] == expected_type:
            with packet_succ_lock:
                packets_sent_successfully.add(data[5:9])

            timer = time.time()

            while expected_num_big_end not in packets_sent_successfully:
                print(f"\t\t\tWaiting for packet No. {expected_number} to be ack'd...")
                time.sleep(0.01)
                if time.time() >= timer + TIMEOUT_ACK:
                    return -3
            return 0
        else:
            return -2
    except socket.timeout:
        return -1


def send_packet(main_socket, addr, packet, exp_type, exp_number):
    with socket_lock:
        main_socket.sendto(packet, addr)
    ret = await_ack(main_socket, exp_type, exp_number)
    while ret != 0:
        print(f"DEBUG: Retrying Packet No. {exp_number}\tType: {exp_type}\tERRCODE: {ret}")
        with socket_lock:
            main_socket.sendto(packet, addr)
        ret = await_ack(main_socket, exp_type, exp_number)


def create_metapacket(filename):
    filehash = hash_generator(filename)
    filesize = str(os.stat(filename).st_size).encode()

    metadata = b'\x00' + b";" + filename.encode() + b";" + filesize + b";" + filehash.encode()
    metapacket = crc32_generate(metadata)
    return metapacket


def create_datapacket(file, data_idx):
    file.seek(data_idx * CHUNK_LEN)
    data_chunk = file.read(CHUNK_LEN)
    if not data_chunk:
        return -1
    data = b"\x01" + data_idx.to_bytes(4, byteorder="big") + data_chunk
    return crc32_generate(data)


def create_stoppacket():
    data = b'\xff'
    return crc32_generate(data)


def send_file_chunk(main_socket, addr, file, data_idx):
    with file_lock:
        datapacket = create_datapacket(file, data_idx)
    if datapacket == -1:
        return -1
    send_packet(main_socket, addr, datapacket, DATAPACKET_TYPE, data_idx)


def main():
    with open(FILENAME, 'rb') as f:
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        client_socket.settimeout(.3)
        client_socket.bind(('', SOURCE_PORT))
        addr = (TARGET_IP, TARGET_PORT)

        metapacket = create_metapacket(FILENAME)
        send_packet(client_socket, addr, metapacket, METAPACKET_TYPE, 0xfffffffe)

        data_idx = 0
        packet_count = os.stat(FILENAME).st_size / CHUNK_LEN

        executor = concurrent.futures.ThreadPoolExecutor(max_workers=WINDOW_SIZE)

        while data_idx < packet_count:
            executor.submit(send_file_chunk, client_socket, addr, f, data_idx)
            print("Launching Thread " + str(data_idx))
            #time.sleep(1)
            data_idx += 1

        executor.shutdown(wait=True)

        stoppacket = create_stoppacket()
        send_packet(client_socket, addr, stoppacket, STOPPACKET_TYPE, 0xffffffff)


if __name__ == "__main__":
    start = time.time()
    main()
    end = time.time()
    print(end - start)
