import hashlib
import zlib
import os


def hash_generator(filename):
    sha1 = hashlib.sha1()  # for hash creation

    with open(filename, 'rb') as f:
        while True:
            data = f.read(65536)  # read by chunks not to overload the RAM with entire files!
            if not data:
                break
            sha1.update(data)
    return sha1.hexdigest()


def crc32_checker(mess):
    crc32_received = mess[0:4]
    data = mess[4:]
    crc32_local = zlib.crc32(data).to_bytes(4, byteorder="big")
    return crc32_received == crc32_local


def crc32_stripper(mess):
    return mess[4:]


def crc32_generate(data):
    data_crc32 = zlib.crc32(data).to_bytes(4, byteorder="big")
    packet = data_crc32 + data
    return packet


def delete_file(filename):
    if os.path.exists(filename):
        os.remove(filename)
