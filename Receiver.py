import random
import socket
import os
import zlib
import hashlib
import utils


def meta_decode(message):
    parts = message[6:].decode("utf-8").split(';')
    filename = parts[0]
    file_size = parts[1]
    sha1_hash = parts[2]
    print(f"Incoming file {filename}, {file_size} bytes")
    return filename, file_size, sha1_hash


def receive_meta(address, message, server_socket):
    address = (address[0], 14001)  # NetDerper odpad
    filename, file_size, sha1_hash = meta_decode(message)
    utils.delete_file(filename)
    server_socket.sendto(utils.crc32_generate(b'\x00' + b'\xff' + b'\xff' + b'\xff' + b'\xfe'), address)
    return filename, sha1_hash


def receive_data(address, file, message, server_socket):
    order = int.from_bytes(message[5:9], "big")
    server_socket.sendto(utils.crc32_generate(b'\x01' + order.to_bytes(4, byteorder="big")), address)
    file.seek(order * CHUNK_LEN)
    file.write(message[9:])
    print(f"Received packet No° {order}")


RECEIVE_PORT = 49854
CHUNK_LEN = 1015


def receive():

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_socket.bind(('', RECEIVE_PORT))
    print(f"Listening on port {RECEIVE_PORT}.")
    while True:
        message, address = server_socket.recvfrom(1024)

        if utils.crc32_checker(message) and (message[4] == 0):

            filename, sha1_hash = receive_meta(address, message, server_socket)
            print("Receiving file")

            with open(filename, 'wb') as file:

                while True:

                    message, address = server_socket.recvfrom(1024)
                    address = (address[0], 14001)  # NetDerper odpad

                    if utils.crc32_checker(message):

                        if message[4] == 1:
                            receive_data(address, file, message, server_socket)

                        elif message[4] == 255:
                            print("Transmission ended.")
                            server_socket.sendto(utils.crc32_generate(b"\xff" + b'\xff' + b'\xff' + b'\xff' + b'\xff'), address)
                            break

                        elif message[4] == 0:
                            server_socket.sendto(utils.crc32_generate(b"B73M"), address)
                            break
                        else:
                            server_socket.sendto(utils.crc32_generate(b"B73M"), address)
                    else:
                        server_socket.sendto(utils.crc32_generate(b'\x00' + b'\x00' + b'\x00' + b'\x00' + b'\x00'), address)

            if sha1_hash != utils.hash_generator(filename):
                os.remove(filename)
                print("Transmission failed. File removed!")


if __name__ == "__main__":
    receive()














































































































































#PSIA > PISA